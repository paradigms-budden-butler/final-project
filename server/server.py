'''Bradley Budden (bbudden) and Megan Butler (mbutle13)'''

import cherrypy
import routes
from citiesController import CitiesController
from usersController import UsersController

class OptionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"]="*"
    cherrypy.response.headers["Access-Control-Allow-Methods"]="GET,PUT,POST,DELETE,OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"]="true"

def start_service():
    '''configures and runs the server'''

    # initialize up controllers
    cCon = CitiesController() # cities controller
    uCon = UsersController() # users controller
    oCon = OptionsController() # options controller

    dispatcher = cherrypy.dispatch.RoutesDispatcher() # use to connect endpoints to controllers

    # connecting endpoints to resources
    
    # citiesController
    dispatcher.connect('get_city_key', '/cities/:identifier', controller=cCon, action='GET_CITY_KEY', conditions=dict(method=['GET']))

    # usersController
    dispatcher.connect('get_user_key', '/users/:username', controller=uCon, action='GET_USER_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('get_hs_index', '/users/hs/', controller=uCon, action='GET_USER_HS', conditions=dict(method=['GET']))
    dispatcher.connect('put_user_key', '/users/:username', controller=uCon, action='PUT_USER_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('post_user_index', '/users/', controller=uCon, action='POST_USER_INDEX', conditions=dict(method=['POST']))
    dispatcher.connect('delete_user_index', '/users/', controller=uCon, action='DELETE_USER_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('delete_user_key', '/users/:username', controller=uCon, action='DELETE_USER_KEY', conditions=dict(method=['DELETE']))

    # CORS related OPTIONS endpoints
    dispatcher.connect('cities_get_key', '/cities/:identifier', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_get_hs', '/users/hs/', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_get_key', '/users/:username', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_put_key', '/users/:username', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_post_index', '/users/', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_delete_index', '/users/', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))
    dispatcher.connect('users_delete_key', '/users/:username', controller=oCon, action="OPTIONS", conditions=dict(method=['OPTIONS']))

    # configuration for the server
    conf = {
        'global' : {
            'server.socket_host' : 'student13.cse.nd.edu',
            'server.socket_port' : 51022
            },
        '/' : {
            'request.dispatch' : dispatcher,
            'tools.CORS.on':True
            }
        }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == "__main__":
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
