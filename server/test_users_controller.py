import unittest
import requests
import json

class TestUsers(unittest.TestCase):

    SITE_URL = 'http://student13.cse.nd.edu:51022'
    print("Testing for server: " + SITE_URL)
    USERS_URL = SITE_URL + '/users/'
    HS_URL = USERS_URL + 'hs/'

    def is_json(self, response):
        try:
            json.loads(response)
            return True
        except ValueError:
            return False
    
    def test_users_key_get(self):
        test_key = "admin"
        full_resource_url = self.USERS_URL + test_key
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        # check that response fields match expected results
        # from the users.csv file
        self.assertEqual(response["username"], test_key)
        self.assertEqual(response["password"], "pass") 
        self.assertEqual(response["hs_30"], 6)
        self.assertEqual(response["hs_15"], 5)
        self.assertEqual(response["hs_10"], 4)
        self.assertEqual(response["hs_5"], 3)
        self.assertEqual(response["hs_3"], 2)
        self.assertEqual(response["hs_1"], 1)
        self.assertEqual(response["result"], "success")
        # test for a user that does not exist
        test_invalid_key = "notAUser"
        full_resource_url = self.USERS_URL + test_invalid_key
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "error")

    def test_users_hs_get(self):
        r = requests.get(self.HS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        # check that the recieved high scores match the
        # expected high scores from the users.csv file
        self.assertEqual(response["result"], "success")
        self.assertEqual(response["hs_30"]["score"], 6)
        self.assertEqual(response["hs_30"]["user"], "admin")
        self.assertEqual(response["hs_15"]["score"], 8)
        self.assertEqual(response["hs_15"]["user"], "sample2")
        self.assertEqual(response["hs_10"]["score"], 9)
        self.assertEqual(response["hs_10"]["user"], "sample3")
        self.assertEqual(response["hs_5"]["score"], 10)
        self.assertEqual(response["hs_5"]["user"], "sample4")
        self.assertEqual(response["hs_3"]["score"], 11)
        self.assertEqual(response["hs_3"]["user"], "sample5")
        self.assertEqual(response["hs_1"]["score"], 12)
        self.assertEqual(response["hs_1"]["user"], "sample6")

    def test_users_key_put(self):
        # check put request functionality
        test_key = "admin"
        new_score = 20
        full_resource_url = self.USERS_URL + test_key
        body = {"mode": "hs_30", "score" : new_score}
        r = requests.put(full_resource_url, data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        # check if user's score was correctly updated
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["username"], test_key)
        self.assertEqual(response["hs_30"], new_score)
        # reset the user's score so users.csv stays the
        # same as it was for other testing purposes
        test_key = "admin"
        new_score = 6
        full_resource_url = self.USERS_URL + test_key
        body = {"mode": "hs_30", "score" : new_score}
        r = requests.put(full_resource_url, data = json.dumps(body))

    def test_users_index_post(self):
        # attempt to add new user to database
        test_username = "newGuy"
        test_password = "abcd"
        body = {"username" : test_username, "password" : test_password}
        r = requests.post(self.USERS_URL, data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        # check that the new user was added
        full_resource_url = self.USERS_URL + test_username
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        self.assertEqual(response["username"], test_username)
        self.assertEqual(response["hs_30"], 0) # scores should be 0
        # delete the new user so other tests pass
        r = requests.delete(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")

    
    # This test will delete all sample users from the database
    # and ruin some of the other tests. We have tested it ourselves
    # already to verify that DELETE_USER_INDEX works. 

    #def test_users_index_delete(self):
    #    # attempt to delete all users
    #    r = requests.delete(self.USERS_URL)
    #    self.assertTrue(self.is_json(r.content.decode()))
    #    response = json.loads(r.content.decode())
    #    self.assertEqual(response["result"], "success")
    #    # check that all users were deleted
    #    test_username1 = "admin"
    #    test_username2 = "sample6"
    #    full_resource_url = self.USERS_URL + test_username1
    #    r = requests.get(full_resource_url)
    #    self.assertTrue(self.is_json(r.content.decode()))
    #    response = json.loads(r.content.decode())
    #    self.assertEqual(response["result"], "error")
    #    full_resource_url = self.USERS_URL + test_username2
    #    r = requests.get(full_resource_url)
    #    self.assertTrue(self.is_json(r.content.decode()))
    #    response = json.loads(r.content.decode())
    #    self.assertEqual(response["result"], "error")

    def test_users_key_delete(self):
        # attempt to delete user
        test_username = "sample6"
        full_resource_url = self.USERS_URL + test_username
        r = requests.delete(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        # verify that user was deleted
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "error")
        # add user back to users.csv so that other tests don't fail
        password = "pass"
        body = {"username" : test_username, "password" : password}
        r = requests.post(self.USERS_URL, data=json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        # update the scores to what the user had before
        body = {"mode" : "hs_1", "score" : 12}
        r = requests.put(full_resource_url, data = json.dumps(body))
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "success")
        

if __name__ == "__main__":
    unittest.main()
