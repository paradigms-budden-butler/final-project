from cities_library import _cities_database
from users_library import _users_database
import unittest
import in_place

class TestCitiesLibrary(unittest.TestCase):
    CD = _cities_database()

    def test_generate_city_identifier(self):
        city = "Madison"
        state = "Wisconsin"
        expected_identifier = "madison_wisconsin"
        actual_identifier = self.CD.generate_city_identifier(city, state)
        self.assertEqual(expected_identifier, actual_identifier)
    
    def test_load_cities(self):
        self.CD.cities.clear()
        self.CD.load_cities("cities.csv")
        identifier_to_test = "madison_wisconsin"
        expected_city = "Madison"
        expected_state = "Wisconsin"
        expected_population = 233209
        self.assertEqual(expected_city, self.CD.cities[identifier_to_test]["city"])
        self.assertEqual(expected_state, self.CD.cities[identifier_to_test]["state"])
        self.assertEqual(expected_population, self.CD.cities[identifier_to_test]["population"])
    
    def test_get_city(self):
        self.CD.cities.clear()
        self.CD.load_cities("cities.csv")
        expected_city = "Green Bay"
        expected_state = "Wisconsin"
        expected_population = 104057
        actual_info = self.CD.get_city("greenbay_wisconsin")
        self.assertEqual(expected_city, actual_info["city"])
        self.assertEqual(expected_state, actual_info["state"])
        self.assertEqual(expected_population, actual_info["population"])
        invalid_identifier = "mad ison_wisconsin"
        actual_info = self.CD.get_city(invalid_identifier)
        self.assertEqual(actual_info, None)

class UsersLibraryTest(unittest.TestCase):
    UD = _users_database()

    def test_load_users(self):
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        username_to_test = "admin"
        expected_password = "pass"
        expected_hs_30 = 6
        expected_hs_15 = 5
        expected_hs_10 = 4
        expected_hs_5 = 3
        expected_hs_3 = 2
        expected_hs_1 = 1
        self.assertEqual(expected_password, self.UD.users[username_to_test]["password"])
        self.assertEqual(expected_hs_30, self.UD.users[username_to_test]["hs_30"])
        self.assertEqual(expected_hs_15, self.UD.users[username_to_test]["hs_15"])
        self.assertEqual(expected_hs_10, self.UD.users[username_to_test]["hs_10"])
        self.assertEqual(expected_hs_5, self.UD.users[username_to_test]["hs_5"])
        self.assertEqual(expected_hs_3, self.UD.users[username_to_test]["hs_3"])
        self.assertEqual(expected_hs_1, self.UD.users[username_to_test]["hs_1"])

    def test_get_user(self):
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        username_to_test = "admin"
        expected_username = "admin"
        expected_hs_30 = 6
        expected_hs_15 = 5
        expected_hs_10 = 4
        expected_hs_5 = 3
        expected_hs_3 = 2
        expected_hs_1 = 1
        actual_info = self.UD.get_user(username_to_test)
        self.assertEqual(expected_username, actual_info["username"])
        self.assertEqual(expected_hs_30, actual_info["hs_30"])
        self.assertEqual(expected_hs_15, actual_info["hs_15"])
        self.assertEqual(expected_hs_10, actual_info["hs_10"])
        self.assertEqual(expected_hs_5, actual_info["hs_5"])
        self.assertEqual(expected_hs_3, actual_info["hs_3"])
        self.assertEqual(expected_hs_1, actual_info["hs_1"])

    def test_get_hs(self):
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        expected_hs_30 = 6
        expected_user_30 = "admin"
        expected_hs_15 = 8
        expected_user_15 = "sample2"
        expected_hs_10 = 9
        expected_user_10 = "sample3"
        expected_hs_5 = 10
        expected_user_5 = "sample4"
        expected_hs_3 = 11
        expected_user_3 = "sample5"
        expected_hs_1 = 12
        expected_user_1 = "sample6"
        actual_info = self.UD.get_hs()
        # check scores
        self.assertEqual(expected_hs_30, actual_info["hs_30"]["score"])
        self.assertEqual(expected_hs_15, actual_info["hs_15"]["score"])
        self.assertEqual(expected_hs_10, actual_info["hs_10"]["score"])
        self.assertEqual(expected_hs_5, actual_info["hs_5"]["score"])
        self.assertEqual(expected_hs_3, actual_info["hs_3"]["score"])
        self.assertEqual(expected_hs_1, actual_info["hs_1"]["score"])
        # check usernames
        self.assertEqual(expected_user_30, actual_info["hs_30"]["user"])
        self.assertEqual(expected_user_15, actual_info["hs_15"]["user"])
        self.assertEqual(expected_user_10, actual_info["hs_10"]["user"])
        self.assertEqual(expected_user_5, actual_info["hs_5"]["user"])
        self.assertEqual(expected_user_3, actual_info["hs_3"]["user"])
        self.assertEqual(expected_user_1, actual_info["hs_1"]["user"])

    def test_update_score(self):
        # save original state of csv file
        f = open("users.csv")
        original_lines = []
        for line in f:
            original_lines.append(line)
        f.close()
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        test_username = "admin"
        test_mode = "hs_5"
        test_score = 15
        self.UD.update_score(test_username, test_mode, test_score, "users.csv")
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        self.assertEqual(self.UD.users[test_username][test_mode], test_score)
        # reset users.csv file because update_score modifies it
        f = open("users.csv", "w")
        for line in original_lines:
            f.write(line)
        f.close()
            
    def test_add_user(self):
        # save original state of csv file
        f = open("users.csv")
        original_lines = []
        for line in f:
            original_lines.append(line)
        f.close()
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        test_username = "new_guy"
        test_password = "qwerty"
        self.UD.add_user(test_username, test_password)
        # check that the user was added dynamically
        self.assertEqual(self.UD.users[test_username]["username"], test_username)
        self.assertEqual(self.UD.users[test_username]["password"], test_password)
        self.assertEqual(self.UD.users[test_username]["hs_3"], 0)
        # check that the user was added permanently
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        self.assertEqual(self.UD.users[test_username]["username"], test_username)
        self.assertEqual(self.UD.users[test_username]["password"], test_password)
        self.assertEqual(self.UD.users[test_username]["hs_3"], 0)
        # reset users.csv file because add_user modifies it
        f = open("users.csv", "w")
        for line in original_lines:
            f.write(line)
        f.close()
            
    def test_delete_users(self):
        # save original state of csv file
        f = open("users.csv")
        original_lines = []
        for line in f:
            original_lines.append(line)
        f.close()
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        # check that dynamic users dictionary was cleared
        self.UD.delete_users()
        self.assertEqual(len(self.UD.users), 0)
        # check that users.csv was cleared
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        self.assertEqual(len(self.UD.users), 0)
        # reset users.csv file because delete_users modifies it
        f = open("users.csv", "w")
        for line in original_lines:
            f.write(line)
        f.close()

    def test_delete_user(self):
        # save original state of csv file
        f = open("users.csv")
        original_lines = []
        for line in f:
            original_lines.append(line)
        f.close()
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        test_username = "admin"
        self.UD.delete_user(test_username)
        # test user was deleted from dynamic users dictionary
        self.assertTrue(test_username not in self.UD.users)
        # test user was deleted from users.csv
        self.UD.users.clear()
        self.UD.load_users("users.csv")
        self.UD.delete_user(test_username)
        self.assertTrue(test_username not in self.UD.users)
        # reset users.csv file because delete_user modifies it
        f = open("users.csv", "w")
        for line in original_lines:
            f.write(line)
        f.close()

if __name__ == "__main__":
    unittest.main()
