# OO API Features
- Can retrieve city information (get_city)
- Can retrieve overall high scores for each time mode (get_hs)
- Can retrieve scores for a specific user for each time mode (get_user)
- Can update a user's high score for a specific time mode (update_score)
- Can create a new user (add_user)
- Can delete a user (delete_user)
- Can delete all users (delete_users)

# How to Test the OO API
- Be in the 'server' directory of the project repository.
- Simply run "python3 test_api.py"
- This will test the functionality of both cities_library.py and users_library.py.

# How to Test the Server
- Be on the student13 machine.
- Be in the 'server' directory of the project repository.
- Start the server with "python3 server.py"
- From another session, be in the same directory and run "python3 test_cities_controller.py" and "python3 test_users_controller.py"
- This will test the functionality of both controllers and the server.

# Notes
- The machine used to test the server must have the cherrypy module.

# JSON Specification
| Request Type | Resource Endpoint                                  | Body                                             | Expected Response                                                                                                                                                                         | Inner Workings of Handler                                                                                                                      |
|--------------|----------------------------------------------------|--------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| GET          | /cities/:identifier e.g. /cities/madison_wisconsin | There should be no body to a GET request         | {“result”: “success”, “city”: “Madison”, “state”: “Wisconsin”, “population”: 233209} if the operation worked                                                                              | GET_CITY_KEY. Goes through all of the city_state combinations looking for a match. Uses get_city()                                             |
| GET          | /users/hs/                                         | There should be no body to a GET request         | {“result”: “success”,”hs_30”: { “score”: 221, “user”: “bestGuesser”}, “hs_15”: {“score”: 167, “user”: “jeff”}, … , “hs_1”: {“score”:18, “user”: “knowsItAll”} } , if the operation worked | GET_USER_HS. Goes through and finds the top score for each of the six time modes, returning the scores and corresponding users. Uses get_hs(). |
| GET          | /users/:username                                   | There should be no body to a GET request         | {“result”: “success”, “username”: “geoWizard”, “password”: “pass”, “hs_30”: “123”, “hs_15”: “55”, …} if the operation worked                                                              | GET_USER_KEY. Gets user info and scores for each of the 6 timed modes. Uses get_user()                                                         |
| PUT          | /users/:username                                   | ‘{“mode”: “hs_30”, “score” : 198}’               | {“result”: “success”} if the operation worked                                                                                                                                             | PUT_USER_KEY. Updates a highscore in the database for the specified user. Uses update_score()                                                  |
| POST         | /users/                                            | ‘{“username”: “geoWizard”, “password”: “qwerty”}’ | {“result”: “success”} if the operation worked                                                                                                                                             | POST_USER_INDEX.  Adds a user to the database. Uses add_user()                                                                                 |
| DELETE       | /users/                                            | ‘{}’                                             | {“result”: “success”} if the operation worked                                                                                                                                             | DELETE_USER_INDEX. Deletes all users from the database. Uses delete_users()                                                                    |
| DELETE       | /users/:username                                   | ‘{}’                                             | {“result”: “success”} if the operation worked                                                                                                                                             | DELETE_USER_KEY Deletes specified user from the database. Uses delete_user()                                                                   |
