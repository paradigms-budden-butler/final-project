class _cities_database:

    def __init__(self):
        self.cities = dict()

    def generate_city_identifier(self, city, state):
        city = city.replace(" ", "")
        city = city.lower()
        state = state.replace(" ", "")
        state = state.lower()
        identifier = city + "_" + state
        return identifier

    def load_cities(self, cities_file):
        f = open(cities_file)
        for line in f:
            line = line.strip()
            components = line.split(',')
            city = components[0]
            state = components[1]
            population = int(components[2])
            identifier = self.generate_city_identifier(city, state)
            self.cities[identifier]= {"city": city, "state":state, "population": population}
        f.close()

    def get_city(self, identifier):
        '''Returns a dictionary containing city, state, and population fields for the specified city'''
        try:
            city = self.cities[identifier]
            return city
        except KeyError:
            return None

    