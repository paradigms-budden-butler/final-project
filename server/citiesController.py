import cherrypy
import re, json
from cities_library import _cities_database

class CitiesController(object):

    def __init__(self):
        self.cdb = _cities_database()
        self.cdb.load_cities('cities.csv')

    def GET_CITY_KEY(self, identifier):
        '''Respond to GET request for /cities/:identifier with a JSON formatted string'''
        output = {"result": "success"}
        # get city details
        try:
            city = self.cdb.get_city(identifier)
            output["city"] = city["city"]
            output["state"] = city["state"]
            output["population"] = city["population"]
        except Exception as ex:
            output["result"] = "error"
            output["message"] = "city does not exist in database"
        return json.dumps(output)
