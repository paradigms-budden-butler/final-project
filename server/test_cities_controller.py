import unittest
import requests
import json

class TestCities(unittest.TestCase):

    SITE_URL = 'http://student13.cse.nd.edu:51022'
    print("Testing for server: " + SITE_URL)
    CITIES_URL = SITE_URL + '/cities/'

    def is_json(self, response):
        try:
            json.loads(response)
            return True
        except ValueError:
            return False
    
    def test_cities_key_get(self):
        test_key = "madison_wisconsin"
        full_resource_url = self.CITIES_URL + test_key
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["city"], "Madison")
        self.assertEqual(response["state"], "Wisconsin")
        self.assertEqual(response["population"], 233209)
        self.assertEqual(response["result"], "success")
        test_invalid_key = "mad5ison_wisconsin"
        full_resource_url = self.CITIES_URL + test_invalid_key
        r = requests.get(full_resource_url)
        self.assertTrue(self.is_json(r.content.decode()))
        response = json.loads(r.content.decode())
        self.assertEqual(response["result"], "error")
        self.assertEqual(response["message"], "city does not exist in database")


if __name__ == "__main__":
    unittest.main()
