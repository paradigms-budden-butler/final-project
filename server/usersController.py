import cherrypy
import re, json
from users_library import _users_database

class UsersController(object):

    def __init__(self):
        self.udb = _users_database()
        self.udb.load_users('users.csv')

    def GET_USER_KEY(self, username):
        '''Respond to GET request for /users/:username with a JSON formatted string'''
        output = {"result": "success"}
        # get user details
        try:
            user = self.udb.get_user(username)
            output["username"] = user["username"]
            output["password"] = user["password"]
            output["hs_30"] = user["hs_30"]
            output["hs_15"] = user["hs_15"]
            output["hs_10"] = user["hs_10"]
            output["hs_5"] = user["hs_5"]
            output["hs_3"] = user["hs_3"]
            output["hs_1"] = user["hs_1"]
        except Exception as ex:
            output["result"] = "error"
            output["message"] = "user does not exist in database"
            output["message2"] = str(ex)
        return json.dumps(output)

    def GET_USER_HS(self):
        '''Respond to GET request for /users/hs/ with a JSON formatted string'''
        # get high score details
        try:
            highscores = self.udb.get_hs()
            highscores.update({"result" : "success"})
            return json.dumps(highscores)
        except Exception as ex:
            output["result"] = "error"
            output["message"] = "error when gathering high scores"
            return json.dumps(output)
           
    def PUT_USER_KEY(self, username):
        '''Handle PUT request for /users/:username by updating their score for one of
        the modes communicated through the request body'''
        output = {"result": "success"}
        data_json = json.loads(cherrypy.request.body.read().decode('utf-8'))
        time_mode = data_json["mode"]
        new_score = data_json["score"]
        try:
            self.udb.update_score(username, time_mode, int(new_score), "users.csv")
        except Exception as ex:
            output["result"] = "error"
            output["message"] = "error updating user score with update_score"
        return json.dumps(output)

    def POST_USER_INDEX(self):
        '''Handle POST request for /users/ by adding new user to the database'''
        output = {"result" : "success"}
        data_json = json.loads(cherrypy.request.body.read().decode('utf-8'))
        username = data_json["username"]
        password = data_json["password"]
        try:
            self.udb.add_user(username, password)
        except Exception as ex:
            output["result"] = "error"
            output["message"] = "error adding user to database"
        return json.dumps(output)

    def DELETE_USER_INDEX(self):
        '''Handle DELETE request for /users/ by deleting all users'''
        output = {"result" : "success"}
        try:
            self.udb.delete_users()
        except Exception:
            output["result"] = "error"
            output["message"] = "error deleting all users from database"
        return json.dumps(output)
            
    def DELETE_USER_KEY(self, username):
        '''Handle DELETE request for /users/:username by deleting the user'''
        output = {"result" : "success"}
        try:
            self.udb.delete_user(username)
        except Exception:
            output["result"] = "error"
            output["message"] = "error deleting user from the database"
        return json.dumps(output)






