class _users_database:
    
    def __init__(self):
        self.users = dict()

    def load_users(self, users_file):
        '''Loads user data from csv file into the 'users' dictionary'''
        f = open(users_file)
        for line in f:
            info = line.strip().split(",")
            username = info[0]
            password = info[1]
            hs_30 = int(info[2])
            hs_15 = int(info[3])
            hs_10 = int(info[4])
            hs_5 = int(info[5])
            hs_3 = int(info[6])
            hs_1 = int(info[7])
            self.users[username] = {"username": username, "password": password,
                                    "hs_30": hs_30, "hs_15": hs_15, "hs_10": hs_10, 
                                    "hs_5": hs_5, "hs_3": hs_3, "hs_1": hs_1}
        f.close()
    
    def get_user(self, username):
        '''Returns dictionary of the user's scores for each of the 6 timed modes (and username)'''
        user_info = self.users[username].copy()
        # user_info.pop("password") # we will return the password to allow for authentication
        return user_info
        
    def get_hs(self):
        '''Returns dictionary of the high scores for each of the 6 modes (and usernames for each)'''
        hs_dict = {}
        max_30 = max_15 = max_10 = max_5 = max_3 = max_1 = 0
        for username in self.users:
            user = self.users[username]
            if user["hs_30"] > max_30:
                max_30 = user["hs_30"]
                hs_dict.update({"hs_30": {"score": user["hs_30"], "user": user["username"]}})
            if user["hs_15"] > max_15:
                max_15 = user["hs_15"]
                hs_dict.update({"hs_15": {"score": user["hs_15"], "user": user["username"]}})
            if user["hs_15"] > max_15:
                max_15 = user["hs_15"]
                hs_dict.update({"hs_15": {"score": user["hs_15"], "user": user["username"]}})
            if user["hs_10"] > max_10:
                max_10 = user["hs_10"]
                hs_dict.update({"hs_10": {"score": user["hs_10"], "user": user["username"]}})
            if user["hs_5"] > max_5:
                max_5 = user["hs_5"]
                hs_dict.update({"hs_5": {"score": user["hs_5"], "user": user["username"]}})
            if user["hs_3"] > max_3:
                max_3 = user["hs_3"]
                hs_dict.update({"hs_3": {"score": user["hs_3"], "user": user["username"]}})
            if user["hs_1"] > max_1:
                max_1 = user["hs_1"]
                hs_dict.update({"hs_1": {"score": user["hs_1"], "user": user["username"]}})
        return hs_dict


    def update_score(self, username, mode, score, users_file):
        '''Takes in username, timed mode (i.e. 'hs_5'), and new score for that mode
        and updates the users dictionary (note: new score can be lower than previous score'''
        if mode != "hs_30" and mode != "hs_15" and mode != "hs_10" and mode != "hs_5" and mode != "hs_3" and mode != "hs_1":
            return
        
        # update current dynamic users dictionary
        score = int(score) # make sure score is an int
        self.users[username].update({mode: score})
        # permanently update users.csv file
        # first remove user from users.csv
        user_to_update = []
        score = str(score)
        with open(users_file, "r+") as f:
            d = f.readlines()
            f.seek(0)
            for user in d:
                user = user.strip().split(",")
                if user[0] != username:
                    user = ",".join(user)
                    f.write(user + '\n')
                else:
                    user_to_update = user
            f.truncate()
        # then change user score for given mode
        if mode == "hs_30":
            user_to_update[2] = score
        if mode == "hs_15":
            user_to_update[3] = score
        if mode == "hs_10":
            user_to_update[4] = score
        if mode == "hs_5":
            user_to_update[5] = score
        if mode == "hs_3":
            user_to_update[6] = score
        if mode == "hs_1":
            user_to_update[7] = score
        # then add updated user to users.csv
        with open(users_file, "a") as f:
            user_to_update = ",".join(user_to_update)
            f.write(user_to_update + '\n')

    def add_user(self, username, password):
        '''Takes in a username and a password, creates a user, and adds the user to the database'''
        if username not in self.users:
            # update current dynamic users dictionary
            self.users[username] = {"username": username, "password": password,
                                "hs_30": 0, "hs_15": 0, "hs_10": 0, 
                                "hs_5": 0, "hs_3": 0, "hs_1": 0}
            # permanently update users.csv file
            f = open("users.csv", "a")
            new_user = username + "," + password + ",0,0,0,0,0,0"
            f.write(new_user)
            f.close()

    def delete_users(self):
        '''Deletes all users from the database'''
        # delete all users from current dynamic users dictionary
        self.users.clear()
        # delete all users permanently from users.csv file
        f = open("users.csv", "w")
        f.write("")
        f.close()

    def delete_user(self, username):
        '''Deletes a single user from the database'''
        # check user exists
        try:
            user = self.users[username]
        except Exception as ex:
            return
        # delete user from current dynamic users dictionary
        self.users.pop(username)
        # delete user from users.csv file
        with open(users_file, "r+") as f:
            d = f.readlines()
            f.seek(0)
            for user in d:
                user = user.strip().split(",")
                if user[0] != username:
                    user = ",".join(user)
                    f.write(user + '\n')
                else:
                    continue
            f.truncate()