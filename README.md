# Final Project

**Authors**: Bradley Budden (bbudden) and Megan Butler (mbutle13)

**Description**: 
Our final project is called "USA City Guesser," and it allows users to guess as many US cities/towns as they can think of in one of six different timed modes. The python server maintains a database of US cities/towns, the states they belong to, and their populations. It also maintains a database of users of the site, storing each user's username, password, and scores for each mode between visits to the site. The HTML and JavaScript frontend allows users to maintain a game session and tracks and displays the users' best scores in each mode, as well as the top scores for each mode among all USA City Guesser users. Furthermore, the frontend will add pinpoints to an embedded map whenever the user correctly guesses a US city/town. <mark>To learn more about the server and its JSON specification, as well as the functionality of the OO API it uses, check out the [README](./server/README.md) in the server directory. To learn more about the front end and see screenshots of it in use, check out the [README](./jsfrontend/README.md) in the jsfrontend directory.</mark>

**Complexity and Scale**:
We believe that our project is at a medium level of complexity. We not only parsed data and updated the frontend based on that data but also incorporated user login and score saving functionalities. In addition, we utilized third party APIs from Google in order to embed the interactive map and find and mark the geographic locations of cities that users named correctly. 

In terms of scale, we believe our project is sufficient but not necessarily as large as it could be expanded to become. Currently, our project presents information about city locations and populations, but it could theoretically show much more information, such as income levels, race distribution, etc. Additionally, while we feel our project provides a fun, easy-to-use game that users will want to come back to, we could also offer more variations of games, such as state capital guessers, country guessers, etc. 


**Presentation**: Follow [this link](https://docs.google.com/presentation/d/1lO3u9GLg2dYyhQD4QPV07A6_ANyJang9aqNeZcZb3ac/edit?usp=sharing) to view our presentation slides. 