"use strict";

var PORT = "51022";
var DOMAIN = "http://student13.cse.nd.edu";
var URL_USERS = DOMAIN + ":" + PORT + "/users/";
var URL_CITIES = DOMAIN + ":" + PORT + "/cities/";
var URL_HS = URL_USERS + "hs/";
var USA_POPULATION_2010 = 309300000;
var USER_LOGGED_IN = false;
var USERNAME = null;
var POPULATION_GUESSED = 0;
var CITIES_ALREADY_GUESSED = new Set();
var MAP;
var GEOCODER;

window.onload = function () {
	load_global_leaderboard();
	// clear modals before opening
	document.getElementById("homepage-login-button").onclick = clear_login_modal;
	document.getElementById("homepage-create-account-button").onclick = clear_create_account_modal;
	// attempt login after submit button pressed within login modal window
	document.getElementById("modal-login-button").onclick = login;
	// start a new game
	document.getElementById("bsr-startGame-button").onclick = start_game;
	// reset the game
	document.getElementById("bsr-reset-button").onclick = reset_game;
	// yes- log out button
	document.getElementById("logout-form-yes-button").onclick = log_user_out;
	// no - stay logged in button
	document.getElementById("logout-form-no-button").onclick = stay_logged_in;
	// attempt to create account
	document.getElementById("modal-create-account-button").onclick = create_account;
	// check guess
	document.getElementById("bsr-submit-button").onclick = check_guess;
	// initialize map
	initialize_map();
}

window.onresize = function() {
	// set height and width of map
    var mapDiv = document.getElementById("map");
    var height = window.innerHeight / 1.7;
    var width = window.innerWidth / 1.85;
    console.log("height: " + height + ", width: " + width);
    mapDiv.style.height = `${height}px`;
    mapDiv.style.width = `${width}px`;
};

// prevent form submissions with enter (which reloads the page),
// instead redirect it to check_guess function
$('form input').keydown(function (e) {
    if (e.keyCode == 13) {
		e.preventDefault();
		check_guess();
        return false;
    }
});

// Utility Functions
function is_valid_username(str) {
	var code, i, len;
	for (i = 0, len = str.length; i < len; i++) {
		code = str.charCodeAt(i);
		if (!(code > 47 && code < 58) && // numeric (0-9)
			!(code > 64 && code < 91) && // upper alpha (A-Z)
			!(code > 96 && code < 123) && // lower alpha (a-z)
			!(code == 33) &&	// exclamation point (!)
			!(code == 45) &&    // dash (-)
			!(code == 95) &&    // underscore (_) 
			!(code == 64)){ 	// at sign (@)
			return false;
		}
	}
	return true;
}

function load_global_leaderboard() {
	var xhr = new XMLHttpRequest();
	var url = URL_HS;

	xhr.onload = function (e) {
        var response_text = xhr.responseText;
        console.log(response_text);
        var response_json = JSON.parse(response_text);
        if (response_json["result"] == "error") { // some error with server
            document.getElementById("global-leaderboard-error").style.display = "inline";
        }
        else { // received good response from server
			// display global leaderboard info
			// users
			document.getElementById("global-best-user-1").innerHTML = response_json["hs_1"]["user"];
			document.getElementById("global-best-user-3").innerHTML = response_json["hs_3"]["user"];
			document.getElementById("global-best-user-5").innerHTML = response_json["hs_5"]["user"];
			document.getElementById("global-best-user-10").innerHTML = response_json["hs_10"]["user"];
			document.getElementById("global-best-user-15").innerHTML = response_json["hs_15"]["user"];
			document.getElementById("global-best-user-30").innerHTML = response_json["hs_30"]["user"];
			// their scores
            document.getElementById("global-best-score-1").innerHTML = response_json["hs_1"]["score"];
            document.getElementById("global-best-score-3").innerHTML = response_json["hs_3"]["score"];
            document.getElementById("global-best-score-5").innerHTML = response_json["hs_5"]["score"];
            document.getElementById("global-best-score-10").innerHTML = response_json["hs_10"]["score"];
            document.getElementById("global-best-score-15").innerHTML = response_json["hs_15"]["score"];
            document.getElementById("global-best-score-30").innerHTML = response_json["hs_30"]["score"];
        }
    }

    xhr.onerror = function (e) { // some error with request
        console.log(xhr.statusText);
        document.getElementById("global-leaderboard-error").style.display = "inline";
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}