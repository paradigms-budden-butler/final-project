"use strict";

function clear_login_modal() {
    var username_textbox = document.getElementById("input-username-7755672");
    username_textbox.value = "";
    var password_textbox = document.getElementById("input-password-199339");
    password_textbox.value = "";
    var empty_username_error = document.getElementById("login-empty-username-p");
    var invalid_username_error = document.getElementById("login-invalid-username-p");
    var empty_password_error = document.getElementById("login-empty-password-p");
    var user_not_exist = document.getElementById("login-user-not-exist-p");
    empty_username_error.style.display = "none";
    invalid_username_error.style.display = "none";
    empty_password_error.style.display = "none";
    user_not_exist.style.display = "none";
}

function login() {
    //reset 'incorrect username/password combination' error 
    var user_not_exist = document.getElementById("login-user-not-exist-p");
    user_not_exist.style.display = "none";

    var must_return_early = false;

    // read username and password
    var username_textbox = document.getElementById("input-username-7755672");
    var username = username_textbox.value;
    var password_textbox = document.getElementById("input-password-199339");
    var password = password_textbox.value;
    // check that both fields are nonempty
    var empty_username_error = document.getElementById("login-empty-username-p");
    var empty_password_error = document.getElementById("login-empty-password-p");
    if (username.length == 0) { // empty username
        empty_username_error.style.display = "inline";
        must_return_early = true;
    }
    else { // nonempty username
        empty_username_error.style.display = "none";
    }
    if (password.length == 0) { // empty username
        empty_password_error.style.display = "inline";
        must_return_early = true;
    }
    else { // nonempty username
        empty_password_error.style.display = "none";
    }

    // determine if username is valid
    var invalid_username_error = document.getElementById("login-invalid-username-p");
    if (!is_valid_username(username)) { // invalid username
        invalid_username_error.style.display = "inline";
        must_return_early = true;
    }
    else { // valid username
        invalid_username_error = document.getElementById("login-invalid-username-p");
        invalid_username_error.style.display = "none";
    }

    if (must_return_early) { return; }

    // determine if user exists in database
    check_credentials(username, password);
}

// makes request to server - returns user data if user exists
function check_credentials(username, password) {
    var xhr = new XMLHttpRequest();
    var url = URL_USERS + username;

    xhr.onload = function (e) {
        var response_text = xhr.responseText;
        console.log(response_text);
        var response_json = JSON.parse(response_text);
        var user_not_exist = document.getElementById("login-user-not-exist-p");
        if (response_json["result"] == "error" || response_json["password"] != password) {
            // username does not exist or username/password combination does not exists
            user_not_exist.style.display = "inline";
            return;
        }
        else { // user exists
            // close modal window
            $("#login-modal").modal('hide');
            user_not_exist.style.display = "none";
            // set global variable to know that the user is logged in
            USER_LOGGED_IN = true;
            // set global username
            USERNAME = username;
            // display option for user to view their best scores
            document.getElementById("user-best-1").innerHTML = response_json["hs_1"];
            document.getElementById("user-best-3").innerHTML = response_json["hs_3"];
            document.getElementById("user-best-5").innerHTML = response_json["hs_5"];
            document.getElementById("user-best-10").innerHTML = response_json["hs_10"];
            document.getElementById("user-best-15").innerHTML = response_json["hs_15"];
            document.getElementById("user-best-30").innerHTML = response_json["hs_30"];
            var homepage_best_scores_modal_button = document.getElementById("homepage-your-best-scores-button");
            homepage_best_scores_modal_button.style.display = "inline";
            // remove login and create account modal buttons
            var homepage_log_in_modal_button = document.getElementById("homepage-login-button");
            var homepage_create_account_modal_button = document.getElementById("homepage-create-account-button");
            homepage_log_in_modal_button.style.display = "none";
            homepage_create_account_modal_button.style.display = "none";
            // show log out modal button
            var homepage_log_out_modal_button = document.getElementById("homepage-logout-button");
            homepage_log_out_modal_button.style.display = "inline";
        }
    }

    xhr.onerror = function (e) {
        console.log(xhr.statusText);
        //return undefined;
        return;
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}

// LOGGING OUT FUNCTIONALITY

function stay_logged_in(){
    // close log out modal window
    $("#logout-modal").modal('hide');
}

function log_user_out() {
    // close modal window
    $("#logout-modal").modal('hide');
    // set global variable to know that the user is logged out
    USER_LOGGED_IN = false;
    // reset global username
    USERNAME = null;
    // display log in and create account buttons
    var homepage_log_in_modal_button = document.getElementById("homepage-login-button");
    var homepage_create_account_modal_button = document.getElementById("homepage-create-account-button");
    homepage_log_in_modal_button.style.display = "inline";
    homepage_create_account_modal_button.style.display = "inline";
    // hide user best scores modal table
    var homepage_best_scores_modal_button = document.getElementById("homepage-your-best-scores-button");
    homepage_best_scores_modal_button.style.display = "none";
    // hide logout modal button
    var homepage_log_out_modal_button = document.getElementById("homepage-logout-button");
    homepage_log_out_modal_button.style.display = "none";
}