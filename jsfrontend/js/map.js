function initialize_map() {
    // set height and width of map
    var mapDiv = document.getElementById("map");
    var height = window.innerHeight / 1.7;
    var width = window.innerWidth / 1.9; // document.getElementById("mapWrapper").width;
    console.log("height: " + height + ", width: " + width);
    mapDiv.style.height = `${height}px`;
    mapDiv.style.width = `${width}px`;

    // hide city names on the map
    var mapStyle = [
        {
          featureType: "administrative.locality",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "administrative.neighborhood",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "administrative.land_parcel",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "landscape",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "poi",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "road",
          elementType: "all",
          stylers: [
            { visibility: "off" }
          ]
        },{
          featureType: "transit",
          elementType: "all",
          stylers: [
            { visibility: "off" }
          ]
        }
      ]


    GEOCODER = new google.maps.Geocoder();
    var center = new google.maps.LatLng(39.50, -98.35);
    const USA_BOUNDS = {
        north: 71.5,
        south: 17,
        west: -172.0,
        east: -60.0
    }
    var mapOptions = {
      zoom: 4,
      center: center,
      mapTypeId: 'mapStyle',
      streetViewControl: false,
      fullscreenControl: false,
      restriction: {
        latLngBounds: USA_BOUNDS
      },
      mapTypeControlOptions: { mapTypeIds: [] },
      zoomControl: true
    }
    MAP = new google.maps.Map(document.getElementById('map'), mapOptions);
    MAP.mapTypes.set('mapStyle', new google.maps.StyledMapType(mapStyle, { name: 'Map Style' }));
}

function update_map(city, state) {
    var address = city + ", " + state + ", United States of America";
    //address = encodeURIComponent(address);
    GEOCODER.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        //map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: MAP,
            position: results[0].geometry.location
        });
      } else {
        console.log('Geocode was not successful for the following reason: ' + status);
      }
    });
}

