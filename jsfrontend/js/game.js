"use strict";
var RESET_TIMER = false;

function start_game() {
    // display the game controls
    document.getElementById("guessForm").style.display = 'inline';
    document.getElementById("game-stats-num-cities-p").style.display = "inline";
    document.getElementById("game-stats-percent-p").style.display = "inline";
    // show the reset button
    document.getElementById("bsr-reset-button").style.display = "inline";
    // hide the start game button
    document.getElementById("bsr-startGame-button").style.display = "none";
    // get value of game mode dropdown before hiding
    var selected_game_mode = document.getElementById("select-1831362").value;
    // hide game mode dropdown
    document.getElementById("start-game-form").style.display = "none";
    // show game timer
    document.getElementById("timer-div").style.display = "inline";
    // show game stats
    document.getElementById("game-not-started-text").style.display = "none";
    document.getElementById("game-stats").style.display = "inline";
    // start timer
    start_timer(selected_game_mode);
    RESET_TIMER = false;
}

function reset_game() {
    document.getElementById("guessForm").style.display = 'none';
    // show the start game button
    document.getElementById("bsr-startGame-button").style.display = "inline";
    // hide the reset button
    document.getElementById("bsr-reset-button").style.display = "none";
    // show game mode dropdown
    document.getElementById("start-game-form").style.display = "inline";
    // hide game timer
    document.getElementById("timer-div").style.display = "none";
    // reset stats in Game Stats panel
    document.getElementById("num-named").innerHTML = 0;
    document.getElementById("percent-named").innerHTML = 0;
    POPULATION_GUESSED = 0;
    // then hide game stats
    document.getElementById("game-not-started-text").style.display = "inline";
    document.getElementById("game-stats").style.display = "none";
    // hide text prompt to create an account if it's displaying
    document.getElementById("prompt-user-to-create-account").style.display = "none";
    // hide "Game Over" text in Control Panel if it's displaying
    document.getElementById("game-over-text").style.display = "none";
    // hide "City does not exist in database." text if it's displaying
    document.getElementById("city-not-exist-text").style.display = "none";
    // empty the list of cities the user guessed correctly
    CITIES_ALREADY_GUESSED = new Set();
    // clear the map of location markers
    initialize_map();
    // reset timer
    RESET_TIMER = true;
    // clear any values in the form
    document.getElementById("input-text-6381859").value = "";
    document.getElementById("select-6097770").selectedIndex = 0;
    // hide "already guessed" text
    document.getElementById("city-already-named").style.display = "none";
    
}

function start_timer(game_mode) {
    var time_initial;
    switch (game_mode) {
        case '1':
            time_initial = 60;
            break;
        case '3':
            time_initial = 180;
            break;
        case '5':
            time_initial = 300;
            break;
        case '10':
            time_initial = 600;
            break;
        case '15':
            time_initial = 900;
            break;
        case '30':
            time_initial = 1800;
            break;
    }

    var running_time = [time_initial]
    const wait = time => new Promise(tick => setTimeout(tick, time));
    async function wait_one_second(running_time) {
        running_time[0] -= 1;
        update_timer(running_time[0]);
        await wait(1000);
    }

    async function wrapper() {
        async function decrement_timer() {
            await wait_one_second(running_time).then(async () => {
                if (RESET_TIMER) {
                    // reset_game event handler manages
                    // UI logistics at this point
                    return;
                }
                if (running_time[0] == 0){
                    return;
                }
                await decrement_timer(running_time);
            });
        }
        await decrement_timer();
    }

    wrapper().then( ()=> {
        if (!RESET_TIMER) {
            // don't attempt to save scores if the
            // user reset the game
            handle_end_of_game(game_mode);
        }
    })
    
}

function update_timer(time) {
    var minutes_text = document.getElementById("minutes");
    var seconds_text = document.getElementById("seconds");
    var minutes = Math.floor(time / 60);
    time %= 60;
    var seconds = time;
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    minutes_text.innerHTML = minutes;
    seconds_text.innerHTML = seconds;
}

function check_guess() {
    // get the user's guessed city and state
    var city = document.getElementById("input-text-6381859").value;
    var select = document.getElementById("select-6097770");
    var state = select.options[select.selectedIndex].text;
    // see if city exists
    var xhr = new XMLHttpRequest();
    var city_identifier = generate_city_identifier(city, state);
    var url = URL_CITIES + city_identifier;

    xhr.onload = function (e) {
        // first hide "already guessed" text
        document.getElementById("city-already-named").style.display = "none";
        // if so, add to map and update game stats
        var response = JSON.parse(xhr.responseText);
        if (response["result"] == "error" ) { // city does not exist in database
            document.getElementById("city-not-exist-text").style.display = "inline";
        }
        else{ // city does exist in database
            document.getElementById("city-not-exist-text").style.display = "none";
            // check if the city has already been correctly guessed
            if (CITIES_ALREADY_GUESSED.has(city_identifier)) {
                document.getElementById("city-already-named").style.display = "inline";
                return;
            }
            // add city to cities the user has already guessed
            CITIES_ALREADY_GUESSED.add(city_identifier);
            // increment num-guessed in Game Stats
            var curr_num_guessed = parseInt(document.getElementById("num-named").innerHTML);
            document.getElementById("num-named").innerHTML = curr_num_guessed + 1;
            // recalculate percent-guessed in Game Stats
            POPULATION_GUESSED += response["population"];
            var curr_percent_guessed = parseFloat(((POPULATION_GUESSED / USA_POPULATION_2010) * 100).toFixed(2));
            document.getElementById("percent-named").innerHTML = curr_percent_guessed;
            // update map
            update_map(city, state);
        }
    }

    xhr.onerror = function (e) {
        console.log(xhr.statusText);
        return;
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}

function handle_end_of_game(game_mode) {
    console.log("End of game");
    // Hide timer div
    document.getElementById("timer-div").style.display = "none";
    // display "Game Over" text in Control Panel
    document.getElementById("game-over-text").style.display = "inline";
    // hide form to guess cities
    document.getElementById("guessForm").style.display = "none";
    // If the user was not logged in, don't save scores.
    // Do prompt the user to create an account to 
    // save future scores.
    if (!USER_LOGGED_IN) {
        document.getElementById("prompt-user-to-create-account").style.display = "inline";
    }
    // if the user was logged in, update their best scores if applicable
    else {
        var num_named = parseInt(document.getElementById("num-named").innerHTML);
        switch (game_mode) {
            case '1':
                var user_best_1 = document.getElementById("user-best-1");
                console.log("num_named: " + num_named);
                console.log("user_best_1: " + parseInt(user_best_1.innerHTML));
                if (num_named > parseInt(user_best_1.innerHTML)) {
                    // update the user's best scores table
                    user_best_1.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_1" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
            case '3':
                var user_best_3 = document.getElementById("user-best-3");
                if (num_named > parseInt(user_best_3.innerHTML)) {
                    // update the user's best scores table
                    user_best_3.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_3" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
            case '5':
                var user_best_5 = document.getElementById("user-best-5");
                if (num_named > parseInt(user_best_5.innerHTML)) {
                    // update the user's best scores table
                    user_best_5.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_5" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
            case '10':
                var user_best_10 = document.getElementById("user-best-10");
                if (num_named > parseInt(user_best_10.innerHTML)) {
                    // update the user's best scores table
                    user_best_10.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_10" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
            case '15':
                var user_best_15 = document.getElementById("user-best-15");
                if (num_named > parseInt(user_best_15.innerHTML)) {
                    // update the user's best scores table
                    user_best_15.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_15" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
            case '30':
                var user_best_30 = document.getElementById("user-best-30");
                if (num_named > parseInt(user_best_30.innerHTML)) {
                    // update the user's best scores table
                    user_best_30.innerHTML = num_named;
                    // update the database
                    update_hs("{\"mode\":" + "\"" + "hs_30" + "\"" + ", \"score\": " + num_named + "}")
                }
                break;
        }
    }
}

function update_hs(request_body) {
    var xhr = new XMLHttpRequest();
    var url = URL_USERS + USERNAME;

    xhr.onload = function (e) {
        alert("New high score saved!");
        console.log("response: " + xhr.responseText);
        load_global_leaderboard();
    }

    xhr.onerror = function (e) {
        alert("Sorry, there was an error saving your new high score.");
    }

    xhr.open("PUT", url, true);
    xhr.send(request_body);
}

function generate_city_identifier(city, state) {
    city = city.split(" ").join("");
    state = state.split(" ").join("");
    city = city.toLowerCase();
    state = state.toLowerCase();
    return city + "_" + state;
}