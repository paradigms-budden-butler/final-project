"use strict";

function clear_create_account_modal() {
    var username_textbox = document.getElementById("input-create-username-7755672");
    username_textbox.value = "";
    var password_textbox = document.getElementById("input-create-password-199339");
    password_textbox.value = "";
    var empty_username_error = document.getElementById("create-account-empty-username-p");
    var invalid_username_error = document.getElementById("create-account-invalid-username-p");
    var empty_password_error = document.getElementById("create-account-empty-password-p");
    var create_account_user_already_exists = document.getElementById("create-account-user-already-exists-p");
    var create_user_error = document.getElementById("create-account-error-p");
    empty_username_error.style.display = "none";
    invalid_username_error.style.display = "none";
    empty_password_error.style.display = "none";
    create_account_user_already_exists.style.display = "none";
    create_user_error.style.display = "none";
}

function create_account() {
    //reset 'Error creating account' error 
    var user_already_exists = document.getElementById("create-account-user-already-exists-p");
    user_already_exists.style.display = "none";

    var must_return_early = false;

    // read username and password
    var username_textbox = document.getElementById("input-create-username-7755672");
    var username = username_textbox.value;
    var password_textbox = document.getElementById("input-create-password-199339");
    var password = password_textbox.value;
    // check that both fields are nonempty
    var empty_username_error = document.getElementById("create-account-empty-username-p");
    var empty_password_error = document.getElementById("create-account-empty-password-p");
    if (username.length == 0) { // empty username
        empty_username_error.style.display = "inline";
        must_return_early = true;
    }
    else { // nonempty username
        empty_username_error.style.display = "none";
    }
    if (password.length == 0) { // empty username
        empty_password_error.style.display = "inline";
        must_return_early = true;
    }
    else { // nonempty username
        empty_password_error.style.display = "none";
    }

    // determine if username is valid
    var invalid_username_error = document.getElementById("create-account-invalid-username-p");
    if (!is_valid_username(username)) { // invalid username
        invalid_username_error.style.display = "inline";
        must_return_early = true;
    }
    else { // valid username
        invalid_username_error = document.getElementById("create-account-invalid-username-p");
        invalid_username_error.style.display = "none";
    }

    if (must_return_early) { return; }

    // determine if user exists in database, then create account if not
    check_if_user_exists(username, password);

}

function check_if_user_exists(username, password) {
    // first check if user already exists
    var xhr = new XMLHttpRequest();
    var url = URL_USERS + username;

    xhr.onload = function (e) {
        var response_text = xhr.responseText;
        console.log(response_text);
        var response_json = JSON.parse(response_text);
        if (response_json["result"] == "error") { // user doesn't exist, create new account
            create_new_account(username, password);
        }
        else { // user already exists
            var user_already_exists = document.getElementById("create-account-user-already-exists-p");
            user_already_exists.style.display = "inline";
            return;
        }
    }
    xhr.onerror = function(e) { // issue with server response
        console.log(xhr.statusText);
        return;
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}

function create_new_account(username, password) {
    var xhr = new XMLHttpRequest();
    var url = URL_USERS;

    xhr.onload = function (e) {
        var response_text = xhr.responseText;
        console.log(response_text);
        var response_json = JSON.parse(response_text);
        if (response_json["result"] == "error") { // error creating new account
            var create_user_error = document.getElementById("create-account-error-p");
            create_user_error.style.display = "inline";
            return;
        }
        else {
            // log the user in after the new account is created
            check_credentials(username, password);
            // close create account modal
            $("#create-account-modal").modal('hide');
            alert("New account created successfully. You have been logged in.");
        }
    }
    
    xhr.onerror = function(e) { // issue with server response
        console.log(xhr.statusText);
        return;
    }
    
    xhr.open("POST", url, true);
    xhr.send("{\"username\":" + "\"" + username + "\"" + ", \"password\": " + "\"" + password + "\"" + "}");
}