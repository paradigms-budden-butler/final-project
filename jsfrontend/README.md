# User Interaction
**How to use it:** First, start the server by running "python3 server.py" on the student13.cse.nd.edu machine. Then follow [this link](http://paradigms-budden-butler.gitlab.io/final-project/jsfrontend/html/index.html) to open the front end in your browser (preferably Chrome). If your browser is forcing an https connection (which will not work correctly with the server), try clearing your browser's cached data. You may create an account, log into an existing account, or play the game without logging in. Simply select a gamemode and then name as many U.S. cities as you can in the allotted time. Any cities you name will be marked on the map. Your scores will be automatically saved if you are logged in, and you can also see the global leaderboard at any time.


# Testing the Web Client:
**How we tested it:**
- Login/Create Account/Logout: We first checked that pressing the respective button for each of these options opened a modal that the user could interact with. We then tested all logical edge cases for user input. For the Login and Create Account modals, the JS checks that the entire form was completed, the username consists only of acceptable characters, and, for Create Account specifically, that the provided username does not already exist in the database. We tested each of these cases, and the UI provided proper error messages in each case. We furthermore checked newly created users were actually added to our [users.csv](../server/users.csv) file on the server. For the Logout modal, we made sure that choosing to actually stay logged in or to complete the logout process properly updated global variables in the JS and caused the appropriate buttons to display at the top right. 

- 'Your Best Scores' and 'Global Leaderboard': We made sure that the button to see your best scores only displayed when the user was logged in and that the Your Best Scores modal accurately displayed data from the server, as well as new data from games the user plays during the current session. We checked that the Global Leaderboard got accurately populated when the page first loaded, and that it updated correctly if a logged-in user scored high enough to be on it.

- Game functionality: We tested that the game mode dropdown resulted in the proper game mode after the Start Game button was pressed. We tested to be sure that made-up/incorrect cities neither got matches from the server nor improved the users' scores. We tested to be sure if the user inputs the same city twice it won't be counted twice. We also tested the Start Game and Reset buttons to be sure that the proper UI elements are visible to the user whether they are in a game, just finished a game, or recently reset an ongoing or finished game. We tested that the reset button cleared stats from the previous game and stopped the timer from running.

- Server Requests: Some of these stated functionalities require interaction with our server, and in order to inspect requests and responses between the client and server, we constantly logged these values to the console in order to check for improper request formatting and/or unexpected server responses. 

- Embedded Map: We tested that when cities were correctly named, markers were displayed on the map at the cities' geographic locations. We also tested that resetting a game also cleared the markers and recentered the map. Finally, we tested that changing the window size caused the map to resize accordingly so that components of the page would still fit together correctly. We also had to make sure that city names were not displayed on the map so that users could not simply zoom in and find cities to guess. 

# Screenshots of the Front End:
<div align="center">

On page load:

<img src="./images/frontend_screenshots/on_page_load.JPG" alt="drawing" width="1000"/>

Instructions:

<img src="./images/frontend_screenshots/instructions.JPG" alt="drawing" width="1000"/>

Global Leaderboard:

<img src="./images/frontend_screenshots/global_leaderboard.JPG" alt="drawing" width="1000"/>

Login modal:

<img src="./images/frontend_screenshots/login_modal.JPG" alt="drawing" width="1000"/>

Create Account modal:

<img src="./images/frontend_screenshots/create_account_modal.JPG" alt="drawing" width="1000"/>

On login:

<img src="./images/frontend_screenshots/on_login.JPG" alt="drawing" width="1000"/>

Your Best Scores:

<img src="./images/frontend_screenshots/your_best_scores.JPG" alt="drawing" width="1000"/>

Logout modal:

<img src="./images/frontend_screenshots/logout_modal.JPG" alt="drawing" width="1000"/>

On Game Start:

<img src="./images/frontend_screenshots/on_game_start.JPG" alt="drawing" width="1000"/>

During Game:

<img src="./images/frontend_screenshots/during_game.JPG" alt="drawing" width="1000"/>

On Game Over:

<img src="./images/frontend_screenshots/on_game_over.JPG" alt="drawing" width="1000"/>

</div>
